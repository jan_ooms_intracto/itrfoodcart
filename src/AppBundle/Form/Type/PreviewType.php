<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class PreviewType
{
    public function getName()
    {
        return 'preview';
    }
}