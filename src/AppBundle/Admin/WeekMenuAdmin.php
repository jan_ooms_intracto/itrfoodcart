<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class WeekMenuAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('startDate')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('startDate')
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
//        $formMapper
//            ->tab('User')
//            ->with('Profile', )->end()
//            ->with('General', array('class' => 'col-md-6'))->end()
//            ->with('Social', array('class' => 'col-md-6'))->end()
//            ->end()
//            ->tab('Security')
//            ->with('Status', array('class' => 'col-md-4'))->end()
//            ->with('Groups', array('class' => 'col-md-4'))->end()
//            ->with('Keys', array('class' => 'col-md-4'))->end()
//            ->with('Roles', array('class' => 'col-md-12'))->end()
//            ->end()
//        ;

        $formMapper
            ->with('Algemene gegevens', array('class' => 'col-md-6'))
                ->add('startDate', 'sonata_type_date_picker', array(
                    'dp_side_by_side'       => true,
                    'dp_use_current'        => false,
                    'dp_pick_time'          => false,
                    'label'                 =>'Week start datum'
                ))
                ->add('published', null, array('label'=>'Gepubliceerd'))
            ->end()
            ->with('Maandag', array('class' => 'col-md-6'))
                ->add('mondayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('mondaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Dinsdag', array('class' => 'col-md-6'))
                ->add('tuesdayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('tuesdaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Woensdag', array('class' => 'col-md-6'))
                ->add('wednesdayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('wednesdaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Donderdag', array('class' => 'col-md-6'))
                ->add('thursdayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('thursdaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Vrijdag', array('class' => 'col-md-6'))
                ->add('fridayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('fridaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Zaterdag', array('class' => 'col-md-6'))
                ->add('saturdayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('saturdaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
            ->with('Zondag', array('class' => 'col-md-6'))
                ->add('sundayCourse','sonata_type_model', array('required' => false, 'label' => 'Hoofdgerecht'))
                ->add('sundaySoup','sonata_type_model', array('required' => false, 'label' => 'Dagsoep'))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('startDate')
            ->add('created')
            ->add('updated')
        ;
    }
}
