<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CourseAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('description')
            ->add('created')
            ->add('updated')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('image', null, array(
                'template' => 'AppBundle:Sonata:list_image_preview.html.twig'
            ))
            ->addIdentifier('title')
            ->add('created')
            ->add('updated')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('titelandinfo', array('class'=>'col-md-7'))
            ->add('title')
            ->add('description', 'ckeditor', array(
                'required' => false,
                'trim' => true
            ))
            ->end()
            ->with('image', array('class'=>'col-md-5'))
            ->add('image', 'sonata_type_model_list', array(
                'required' => false
            ), array(
                'link_parameters' => array(
                    'context'  => 'course',
                    'provider' => 'sonata.media.provider.image'
                )
            ))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('image', null, array(
                'template' => 'AppBundle:Sonata:show_image_preview.html.twig'
            ))
            ->add('description', null, array(
                'template' => 'AppBundle:Sonata:show_raw_description.html.twig'
            ))
            ->add('created')
            ->add('updated')
        ;
    }
}
