<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Course;
use AppBundle\Entity\Soup;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WeekMenuRepository")
 * @ORM\Table(name="week_menus")
 * @ORM\HasLifecycleCallbacks
 */
class WeekMenu
{
    /**
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Course
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="monday_course_id", referencedColumnName="id", nullable=true)
     */
    private $mondayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="monday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $mondaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="tuesday_course_id", referencedColumnName="id", nullable=true)
     */
    private $tuesdayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="tuesday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $tuesdaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="wednesday_course_id", referencedColumnName="id", nullable=true)
     */
    private $wednesdayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="wednesday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $wednesdaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="thursday_course_id", referencedColumnName="id", nullable=true)
     */
    private $thursdayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="thursday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $thursdaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="friday_course_id", referencedColumnName="id", nullable=true)
     */
    private $fridayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="friday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $fridaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="saturday_course_id", referencedColumnName="id", nullable=true)
     */
    private $saturdayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="saturday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $saturdaySoup;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Course")
     * @ORM\JoinColumn(name="sunday_course_id", referencedColumnName="id", nullable=true)
     */
    private $sundayCourse;

    /**
     * @var Soup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Soup")
     * @ORM\JoinColumn(name="sunday_soup_id", referencedColumnName="id", nullable=true)
     */
    private $sundaySoup;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    public function __construct()
    {
        $this->published = true;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return WeekMenu
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \AppBundle\Entity\Course $published
     * @return WeekMenu
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getMondayCourse()
    {
        return $this->mondayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $mondayCourse
     * @return WeekMenu
     */
    public function setMondayCourse($mondayCourse)
    {
        $this->mondayCourse = $mondayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getMondaySoup()
    {
        return $this->mondaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $mondaySoup
     * @return WeekMenu
     */
    public function setMondaySoup($mondaySoup)
    {
        $this->mondaySoup = $mondaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getTuesdayCourse()
    {
        return $this->tuesdayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $tuesdayCourse
     * @return WeekMenu
     */
    public function setTuesdayCourse($tuesdayCourse)
    {
        $this->tuesdayCourse = $tuesdayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getTuesdaySoup()
    {
        return $this->tuesdaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $tuesdaySoup
     * @return WeekMenu
     */
    public function setTuesdaySoup($tuesdaySoup)
    {
        $this->tuesdaySoup = $tuesdaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getWednesdayCourse()
    {
        return $this->wednesdayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $wednesdayCourse
     * @return WeekMenu
     */
    public function setWednesdayCourse($wednesdayCourse)
    {
        $this->wednesdayCourse = $wednesdayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getWednesdaySoup()
    {
        return $this->wednesdaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $wednesdaySoup
     * @return WeekMenu
     */
    public function setWednesdaySoup($wednesdaySoup)
    {
        $this->wednesdaySoup = $wednesdaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getThursdayCourse()
    {
        return $this->thursdayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $thursdayCourse
     * @return WeekMenu
     */
    public function setThursdayCourse($thursdayCourse)
    {
        $this->thursdayCourse = $thursdayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getThursdaySoup()
    {
        return $this->thursdaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $thursdaySoup
     * @return WeekMenu
     */
    public function setThursdaySoup($thursdaySoup)
    {
        $this->thursdaySoup = $thursdaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getFridayCourse()
    {
        return $this->fridayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $fridayCourse
     * @return WeekMenu
     */
    public function setFridayCourse($fridayCourse)
    {
        $this->fridayCourse = $fridayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getFridaySoup()
    {
        return $this->fridaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $fridaySoup
     * @return WeekMenu
     */
    public function setFridaySoup($fridaySoup)
    {
        $this->fridaySoup = $fridaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getSaturdayCourse()
    {
        return $this->saturdayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $saturdayCourse
     * @return WeekMenu
     */
    public function setSaturdayCourse($saturdayCourse)
    {
        $this->saturdayCourse = $saturdayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getSaturdaySoup()
    {
        return $this->saturdaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $saturdaySoup
     * @return WeekMenu
     */
    public function setSaturdaySoup($saturdaySoup)
    {
        $this->saturdaySoup = $saturdaySoup;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Course
     */
    public function getSundayCourse()
    {
        return $this->sundayCourse;
    }

    /**
     * @param \AppBundle\Entity\Course $sundayCourse
     * @return WeekMenu
     */
    public function setSundayCourse($sundayCourse)
    {
        $this->sundayCourse = $sundayCourse;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Soup
     */
    public function getSundaySoup()
    {
        return $this->sundaySoup;
    }

    /**
     * @param \AppBundle\Entity\Soup $sundaySoup
     * @return WeekMenu
     */
    public function setSundaySoup($sundaySoup)
    {
        $this->sundaySoup = $sundaySoup;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \DateTime $startDate
     * @return WeekMenu
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return WeekMenu
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     * @return WeekMenu
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function __toString()
    {
        if($this->getStartDate()) {
            return "Weekmenu ".$this->getStartDate()->format('d/m/Y');
        }
        return "New weekmenu";
    }
}