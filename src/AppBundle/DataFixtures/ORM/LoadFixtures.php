<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 13/04/2016
 * Time: 22:59
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Course;
use AppBundle\Entity\Soup;
use AppBundle\Entity\WeekMenu;
use AppBundle\Repository\CourseRepository;
use AppBundle\Repository\SoupRepository;
use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Defines the sample data to load in the database when running the unit and
 * functional tests. Execute this command to load the data:.
 *
 *   $ php app/console doctrine:fixtures:load
 *
 * See http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
 *
 * @author MoriorGames <moriorgames@gmail.com>
 */
class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function load(ObjectManager $manager)
    {
        $this->loadCourses($manager);
        $this->loadSoups($manager);
        $this->loadWeekMenus($manager);
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function loadCourses(ObjectManager $manager)
    {
        $rootDir = $this->container->get('kernel')->getRootDir();
        $imageFixtures = realpath($rootDir.'/Resources/fixtures');

        $media = new Media();
        $media->setContext('course');
        $media->setBinaryContent($imageFixtures.'/spaghetti.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $course = new Course();
        $course
            ->setTitle('Spaghetti Alla Carbonara')
            ->setDescription('<strong>Alla Carbonara</strong><p>Gerookt spek, eigeel, en parmesan</p>')
            ->setImage($media);

        $manager->persist($course);

        $media = new Media();
        $media->setContext('course');
        $media->setBinaryContent($imageFixtures.'/lasagne.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $course = new Course();
        $course
            ->setTitle('Lasagne')
            ->setDescription('<strong>Lasagne van gepocheerde zalm met een mosterdsaus</strong>')
            ->setImage($media);

        $manager->persist($course);

        $media = new Media();
        $media->setContext('course');
        $media->setBinaryContent($imageFixtures.'/chicken_rice.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $course = new Course();
        $course
            ->setTitle('Kip met rijst')
            ->setDescription('<p>Overheerlijke kip met een portie rijst</p>')
            ->setImage($media);

        $manager->persist($course);

        $media = new Media();
        $media->setContext('course');
        $media->setBinaryContent($imageFixtures.'/salmon_and_spicy_broccoli.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $course = new Course();
        $course
            ->setTitle('Zalm met pikante broccoli')
            ->setDescription('<p>Noorse gepocheerde zalm met extra kruidige, pittige broccoli</p>')
            ->setImage($media);

        $manager->persist($course);

        $manager->flush();
    }

    private function loadSoups(ObjectManager $manager)
    {
        $rootDir = $this->container->get('kernel')->getRootDir();
        $imageFixtures = realpath($rootDir.'/Resources/fixtures');

        $media = new Media();
        $media->setContext('soup');
        $media->setBinaryContent($imageFixtures.'/chicken_soup.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $soup = new Soup();
        $soup
            ->setTitle('Kippensoep')
            ->setDescription('<p>Heerlijke kippensoep gemaakt op grootmoeders wijze</p>')
            ->setImage($media);

        $manager->persist($soup);

        $media = new Media();
        $media->setContext('soup');
        $media->setBinaryContent($imageFixtures.'/onion_soup.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $soup = new Soup();
        $soup
            ->setTitle('Uiensoep')
            ->setDescription('<p>Uiensoep met een zeer krokant korstje om van te smullen</p>')
            ->setImage($media);

        $manager->persist($soup);

        $media = new Media();
        $media->setContext('soup');
        $media->setBinaryContent($imageFixtures.'/tomato_soup.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $soup = new Soup();
        $soup
            ->setTitle('Tomatensoep')
            ->setDescription('<p>Tomatensoep met <strong>balletjes</strong>!</p>')
            ->setImage($media);

        $manager->persist($soup);

        $media = new Media();
        $media->setContext('soup');
        $media->setBinaryContent($imageFixtures.'/vegetable_soup.jpg');
        $media->setProviderName('sonata.media.provider.image');
        $soup = new Soup();
        $soup
            ->setTitle('Groentensoep')
            ->setDescription('<p>Groentensoep met verse groentes en pittige kruiding</p>')
            ->setImage($media);

        $manager->persist($soup);

        $manager->flush();
    }

    private function loadWeekMenus(ObjectManager $manager)
    {
        /**
         * @var CourseRepository
         */
        $courseRepo = $manager->getRepository('AppBundle\Entity\Course');

        $allCourses = $courseRepo->findAll();

        /**
         * @var SoupRepository
         */
        $soupRepo = $manager->getRepository('AppBundle\Entity\Soup');
        $allSoups = $soupRepo->findAll();

        $startDate = new \DateTime();
        $startDate->modify('last monday -7 days');

        for($i=0;$i<8;$i++) {
            $startDate = clone($startDate);
            $startDate->modify('next monday');

            $weekMenu = new WeekMenu();

            $weekMenu->setStartDate($startDate);
            $weekMenu->setPublished(true);


            shuffle($allCourses);
            shuffle($allSoups);
            $weekMenu->setMondayCourse($allCourses[0]);
            $weekMenu->setMondaySoup($allSoups[0]);

            shuffle($allCourses);
            shuffle($allSoups);
            $weekMenu->setTuesdayCourse($allCourses[0]);
            $weekMenu->setTuesdaySoup($allSoups[0]);


            shuffle($allCourses);
            shuffle($allSoups);
            $weekMenu->setWednesdayCourse($allCourses[0]);
            $weekMenu->setWednesdaySoup($allSoups[0]);

            shuffle($allCourses);
            shuffle($allSoups);
            $weekMenu->setThursdayCourse($allCourses[0]);
            $weekMenu->setThursdaySoup($allSoups[0]);

            shuffle($allCourses);
            shuffle($allSoups);
            $weekMenu->setFridayCourse($allCourses[0]);
            $weekMenu->setFridaySoup($allSoups[0]);

            $manager->persist($weekMenu);

        }
        $manager->flush();
    }
}
